![alt text](https://i.imgur.com/ElBniIz.png)


# Groceries4Us Project
Groceries4Us is a mobile app made with React-Native for client side
server side with nodejs on [this repo](https://gitlab.com/DorLugasi/groceries4us-server)

The idea of the app is to share groceries lists between members
but the main purpose is to practice modern technologies such as React-Native, NodeJS, MongoDB, CICD 
and deployment to Heroku

<p align="center">
    <img src="https://img.shields.io/badge/license-MIT-brightgreen.svg">
    <img src="https://img.shields.io/badge/contributions-welcome-orange.svg">
</p>

<p align="center">

  
  <a href="#user-content-preview">Preview</a> •
  <a href="#user-content-usage">Usage</a> •
  <a href="#user-content-contributing">Contributing</a> •
  <a href="#user-content-features">Features</a> •
  <a href="#user-content-to-do">To-DO</a> •
  <a href="#user-content-license">License</a>
</p>

## Preview
![alt text](https://i.imgur.com/HWMHDb3.jpg?1)

## Usage

To clone and run this application, you'll need [Git](https://git-scm.com), and [Yarn](https://yarnpkg.com/en/docs/install#windows-stable)
and an Android\iOS emulator or a mobile device

From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/DorLugasi/groceries4us-client.git
# install dependencies
$ yarn
# Run the app
$ yarn start
```


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


## Features:
- [x] Sign In With your Email and password
- [x] Sign Up With your Email and password


## TO-DO:
- [ ] Forget Password Screen
- [ ] Home Screen
  * Show lists
- [ ] Settings
  * Change picture
  * Change password
  * Delete Account
- [ ] History
  * Show previous Lists
  * Clear history (delete all ists permanently )
- [ ] Lists Manipulation
  * Create list
  * Delete list 
  * Add/Remove Items and amount to list
  * Add Recipt
  * Mark as important
  * Set buyers
- [ ] Reports
  * Popular items
  * Least popular items
- [ ] Items auto complete
- [ ] Support multi language

## License

[MIT](https://choosealicense.com/licenses/mit/)

* * *
